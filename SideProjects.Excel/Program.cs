using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideProjects.Excel
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo excelPath = new FileInfo(@"D:\excel\Test.xlsx");

            using (ExcelPackage exl = new ExcelPackage(excelPath))
            {
                ExcelWorksheet workSheet = exl.Workbook.Worksheets.FirstOrDefault();


                for (int i = workSheet.Dimension.Start.Row; i <= workSheet.Dimension.End.Row; i++)
                {
                    object cellValue = workSheet.Cells[i, 2].Value;
                    //for (int j = workSheet.Dimension.Start.Column;
                    //         j <= workSheet.Dimension.End.Column;
                    //         j++)
                    //{
                    //    object cellValue = workSheet.Cells[i, j].Value;
                    //    Console.WriteLine(cellValue);
                    //}
                    Console.WriteLine(cellValue.ToString());
                    if( i == 5)
                    {
                        break;
                    }
                }

            }

            Console.ReadKey();
        }
        
    }
}
