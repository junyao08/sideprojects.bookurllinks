using System;
using System.IO;
using System.IO.Compression;

namespace FolderBackUp
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            try
            {
                var setDateAndTime = DateTime.Now.ToString("dd_MM_yyyy_hhmmtt");
                string zipFolder = "zipFolder.zip";

                //user will enter the whole path/complete path
                string source = args[0];
                string dest = args[1];

                string sourceDirectory = @"" + source;
                string destinationDirectory = @"" + dest + "\\" + setDateAndTime + ".zip";

                string startPath = @"" + source;
                string zipPath = @"" + dest + zipFolder;
                string deletePath = @"" + source + "\\";
                string recreateTemp = @"" + source;

                DirectoryInfo directorySource = new DirectoryInfo(sourceDirectory);
                DirectoryInfo directoryDelete = new DirectoryInfo(deletePath);
                DirectoryInfo recreateTempDirectory = new DirectoryInfo(recreateTemp);
                
                ZipDirectory(startPath, zipPath);
                Directory.Move(zipPath, destinationDirectory);
                DeleteTempDir(directoryDelete);
                Directory.CreateDirectory(recreateTemp);
            }
            catch (Exception ex)
            {
                log4net.Config.BasicConfigurator.Configure();
                log.Error(ex.Message);
            }
            Console.Read();
        }


        public static void ZipDirectory(string fileName, string files)
        {
            Console.WriteLine("Zipping up the files...");
            ZipFile.CreateFromDirectory(fileName, files);
            Console.WriteLine(@"File is zipped up: {0}", files);

        }

        public static void DeleteTempDir(DirectoryInfo deleteDir)
        {
            DirectoryInfo deleteFolder = new DirectoryInfo(deleteDir.FullName);
            deleteFolder.Delete(true);
            Console.WriteLine("Path is deleted: {0}", deleteFolder);
        }
    }
}