﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SideProjects.BookmarkUrlLinks.Models
{
    public class UrlLink
    {
        [Key]
        public int LinkID { get; set; }
        
        [Required(ErrorMessage ="Insert a link")]
        public string Links { get; set; }
    }
}