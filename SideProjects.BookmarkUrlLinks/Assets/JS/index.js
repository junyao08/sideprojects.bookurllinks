
$(document).ready(function () {

    var isFormValid = false;
    var $registerForm = $("form#registerForm");

        console.log($registerForm);

        $registerForm.on("submit", function (e) {
            e.preventDefault();

            var username = $("input[name=Username]").val();
            var password = $("input[name=Password]").val().toUpperCase();

            $.ajax({
                method: "POST",
                url: "register",
                data: {
                    Username: username,
                    Password: password
                }
            }).done(function (data) {
                console.log(data)
            })
        })

    var $error = $("<p>Please ensure your password has caps</p>");
    $("input[name=Password]").on("keyup", function (e) {
        //$(this).val(currVal.toUpperCase());
        var currVal = $(this).val();
        var hasCapsPattern = /([A-Z])/g;

        if (hasCapsPattern.test(currVal)) {
            $error.remove();
            isFormValid = true;
        } else {
            $(this).after($error);
            isFormValid = false;
        }
    })

    $registerForm.on("submit", function (e) {
            e.preventDefault();
            return;
        //return isFormValid;
    })

})



$(document).ready(function () {
    //Delete user without refreshing the page
    $('.delete-url').click(function (e) {
        e.preventDefault();
        var name = $(this).val();
        console.log(name);
        $.ajax({
            type: "POST",
            url: '/api/delete/' + name,
            success: function (data) {
                location.reload();
            },
            error: function (data) {
                console.log('Error', data);
            }
        });
    })
    //hide and show function
    $("#hide").click(function () {
        $(".hideShow").hide(300);
    });

    $("#show").click(function () {
        $(".hideShow").show(300);
    });

    //Fetching API from back-end
    var url = "/api/bookmark";
    var displayApi = document.getElementById("displayApi");
    if (displayApi != null) {
        fetch(url)
            .then(function (response) {
                if (response.status !== 200) {
                    console.log("Looks like there was a proble. Status Code:" + response.status);
                    return;
                }
                response.json().then(function (data) {
                    console.log(data);
                    var link = '';
                    for (var i = 0; i < data.length; i++) {
                        link += '<p>' + data[i].Links + '</p>';
                        //link += 'Links: ' + data[i].Links + '<br/>';
                        //document.('Links: ' + data[i].Links + '</br>');
                    }
                    displayApi.innerHTML = link;
                })
            })
            .catch(function (error) {
                console.log('Fetch Error:-S', error);
            });
    }
})





