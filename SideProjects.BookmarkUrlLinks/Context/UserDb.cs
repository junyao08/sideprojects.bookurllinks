﻿using SideProjects.BookmarkUrlLinks.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SideProjects.BookmarkUrlLinks.Context
{
    public class UserDb : DbContext
    {
        public UserDb() : base("MyDbConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UrlLink> UrlLink { get; set; }
    }
}