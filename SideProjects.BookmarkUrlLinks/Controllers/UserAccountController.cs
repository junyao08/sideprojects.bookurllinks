﻿using SideProjects.BookmarkUrlLinks.Models;
using SideProjects.BookmarkUrlLinks.Context;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using System.Web.Routing;

namespace SideProjects.BookmarkUrlLinks.Controllers
{
    public class UserAccountController : Controller
    {
        [HttpGet]
        [Route("home")]
        public ActionResult Homepage()
        {
            return View("~/Views/UserAccount/Homepage.cshtml");
        }

        [HttpGet]
        [Route("register")]
        public ActionResult CreateUserAccount()
        {
            return View("~/Views/UserAccount/RegisterAccount.cshtml");
        }

        [HttpPost]
        [Route("register")]
        public ActionResult CreateUserAccount(User user)
        {
            //if (user.UserName == null)
            //{
            //    return HttpNotFound("Error: Username is empty");
            //}
            //else if (user.Password == null)
            //{
            //    return HttpNotFound("Error: Password is empty");
            //}

            //check the model binding validation.
            if (ModelState.IsValid)
            {
                using (var db = new UserDb())
                {
                    var newUser = new User()
                    {
                        ID = user.ID,
                        UserName = user.UserName,
                        Password = user.Password
                    };

                    db.Users.Add(newUser);
                    db.SaveChanges();
                }
                return Redirect("get-user");
            }
            else
            {
                //404 Page will be display
                //return HttpNotFound("Error: Register fields are empty");

                List<string> errorMessage = ModelState.Values.SelectMany(v => v.Errors.Select(b => b.ErrorMessage)).ToList();
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, errorMessage.FirstOrDefault());
            }
        }

        [HttpGet]
        [Route("login")]
        public ActionResult Login()
        {
            return View("~/Views/UserAccount/Login.cshtml");
        }

        [HttpPost, ActionName("Login"), Route("login")]
        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
            {
                using (var db = new UserDb())
                {
                    var existingUser = db.Users.Where(a => a.UserName.Equals(user.UserName) && a.Password.Equals(user.Password)).FirstOrDefault();
                    if (existingUser != null)
                    {
                        return Redirect("add-bookmarks");
                    }
                    else
                    {
                        List<string> errorMessage = ModelState.Values.SelectMany(v => v.Errors.Select(b => b.ErrorMessage)).ToList();
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, errorMessage.FirstOrDefault());
                    }
                }
            }
            return View(user);

        }

        [HttpGet]
        [Route("get-user")]
        public ActionResult GetRegisteredUser()
        {
            using (var db = new UserDb())
            {
                var getUser = from u in db.Users
                              orderby u.ID ascending
                              select u;

                var selectedUser = getUser.ToList<User>();

                return View("~/Views/UserAccount/UserRegistered.cshtml", selectedUser);
            }
        }

        [Route("api/delete/{name}")]
        public JsonResult DeleteUser(string name)
        {
            using (var db = new UserDb())
            {
                User getUserID = db.Users.Where(x => x.UserName == name).FirstOrDefault();

                db.Users.Remove(getUserID);
                db.SaveChanges();
            }
            return Json("Success");
        }
    }
}