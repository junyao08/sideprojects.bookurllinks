﻿using SideProjects.BookmarkUrlLinks.Models;
using SideProjects.BookmarkUrlLinks.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SideProjects.BookmarkUrlLinks.Controllers
{
    public class BookmarkUrlController : Controller
    {

        [HttpGet]
        [Route("add-bookmarks")]
        public ActionResult AddBookMark()
        {
            return View("~/Views/BookmarkUrl/AddBookmarkUrl.cshtml");
        }

        [HttpPost]
        [Route("add-bookmarks")]
        public ActionResult AddBookMark(UrlLink link)
        {
            if (ModelState.IsValid)
            {
                using (var db = new UserDb())
                {
                    var newLink = new UrlLink()
                    {
                        LinkID = link.LinkID,
                        Links = link.Links
                    };
                    db.UrlLink.Add(newLink);
                    db.SaveChanges();
                    return Redirect("get-bookmarks");
                }
            }
            else
            {
                return HttpNotFound("Error: Link is empty");
            }
            

        }

        [HttpGet]
        [Route("get-bookmarks")]
        public ActionResult GetLinks()
        {
            using (var db = new UserDb())
            {
                var getLink = from link in db.UrlLink
                              orderby link.LinkID ascending
                              select link;

                var displayLinks = getLink.ToList();

                return View("~/Views/BookmarkUrl/BookmarkUrlLayout.cshtml", displayLinks);

            }
        }


        [Route("delete-bookmarks")]
        public ActionResult DeleteLinksID(int id)
        {
            using (var db = new UserDb())
            {
                UrlLink urlLink = db.UrlLink.Find(id);

                return View("~/Views/BookmarkUrl/DeleteLinks.cshtml", urlLink);
            }
        }

        [HttpPost, ActionName("DeleteLinksID"), Route("delete-bookmarks")]
        public ActionResult DeleteLinks(int id)
        {
           
            using (var db = new UserDb())
            {
                var url = db.UrlLink.Find(id);
                db.UrlLink.Remove(url);
                db.SaveChanges();
            }

            return Redirect("get-bookmarks");
        }


        [HttpGet]
        [Route("html")]
        public ActionResult getHtml()
        {
            return View("~/Views/BookmarkUrl/HTML.cshtml");
        }
    }
}