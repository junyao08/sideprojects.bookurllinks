﻿using SideProjects.BookmarkUrlLinks.Context;
using SideProjects.BookmarkUrlLinks.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace SideProjects.BookmarkUrlLinks.Controllers
{
    public class BookmarkApiController : Controller
    {
        [HttpGet]
        [Route("api/bookmark")]
        public JsonResult GetBookmark()
        {
            using(var db = new UserDb())
            {
                return Json(db.UrlLink.ToList(), JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}
